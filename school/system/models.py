from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models.signals import m2m_changed
from django.core.exceptions import ValidationError

class Course(models.Model):
	name = models.CharField(max_length=100)
	code = models.CharField(max_length=30)

	def __str__(self):
		return str(self.name)

class Subject(models.Model):
	name = models.CharField(max_length=100)
	code = models.CharField(max_length=30)
	year_level = models.PositiveIntegerField(
		validators=[MaxValueValidator(7), MinValueValidator(1)]
	)
	course = models.ManyToManyField(Course)

	def __str__(self):
		return str(self.code)

class Teacher(models.Model):
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	can_teach = models.ManyToManyField(Subject)

	def __str__(self):
		return str(self.first_name)

class Student(models.Model):
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	year_level = models.PositiveIntegerField(
		validators=[MaxValueValidator(7), MinValueValidator(1)]
	)
	course = models.ForeignKey(Course, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.first_name)

class Class(models.Model):
	name = models.CharField(max_length=100)
	max_capacity = models.PositiveIntegerField()
	student = models.ManyToManyField(Student)
	teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
	subject = models.ForeignKey(Subject, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.name)
