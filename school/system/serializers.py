from .models import *
from rest_framework import serializers


class SubjectSerializer(serializers.ModelSerializer):
	class Meta:
		model = Subject
		fields = ('__all__')


class TeacherSerializer(serializers.ModelSerializer):
	class Meta:
		model = Teacher
		fields = ('__all__')


class CourseSerializer(serializers.ModelSerializer):
	class Meta:
		model = Course
		fields = ('__all__')


class StudentSerializer(serializers.ModelSerializer):
	course_details = CourseSerializer(source='course', read_only=True)
	class Meta:
		model = Student
		fields = ('first_name', 'last_name', 'year_level', 'course', 'course_details')


class ClassSerializer(serializers.ModelSerializer):
	teacher_details = TeacherSerializer(source='teacher', read_only=True)
	subject_details = SubjectSerializer(source='subject', read_only=True)
	student_details = StudentSerializer(source="student", read_only=True, many=True)
	#student_details = StudentSerializer(source='student', many=True)
	class Meta:
		model = Class
		fields = ('name', 'max_capacity', 'student', 'teacher', 'subject', 'teacher_details', 'subject_details', 'student_details')

	#def validate(self, data):
	#	if len(data['student']) > data['max_capacity']:
	#		raise serializers.ValidationError("Class is Full!")
	#	return data