import asyncio
import json
from django.contrib.auth import get_user_model
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async
from rest_framework.views import APIView
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response

from .models import*
from .serializers import *

 
# COURSE
class CourseConsumerListCreate(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Course", event)
		await self.send({
			"type": "websocket.accept"
		})

		data = json.loads(json.dumps(await self.list()))
		await self.send({
			"type": "websocket.send",
			"text": repr(data)
		})

	async def websocket_receive(self, event):
		print("received", event)
		get_text = event.get('text', None)
		if get_text is not None:
			data = json.loads(get_text)

		self.create(data)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	@database_sync_to_async
	def list(self):
		queryset = Course.objects.all()
		serializer = CourseSerializer(queryset, many=True)
		return serializer.data

	def create(self, data):
		serializer = CourseSerializer(data=data)
		if serializer.is_valid():
			serializer.save()
		return serializer.data


class CourseConsumerUpdate(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Course", event)
		await self.send({
			"type": "websocket.accept"
		})

	async def websocket_receive(self, event):
		print("received", event)
		get_text = event.get('text', None)
		if get_text is not None:
			data = json.loads(get_text)

		update_id = self.scope['url_route']['kwargs']['pk']
		self.update(data, update_id)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	def update(self, data, update_id):
		queryset = Course.objects.filter(pk=update_id).first()
		serializer = CourseSerializer(queryset, data=data)
		if serializer.is_valid():
			serializer.save()
		return serializer.data


class CourseConsumerDelete(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Course", event)
		await self.send({
			"type": "websocket.accept"
		})

		delete_id = self.scope['url_route']['kwargs']['pk']
		self.delete(delete_id)

	def delete(self, delete_id):
		return Course.objects.filter(id=delete_id).delete()














# SUBJECTS
class SubjectsConsumerListCreate(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Subjects", event)
		await self.send({
			"type": "websocket.accept"
		})

		data = json.loads(json.dumps(await self.list()))
		await self.send({
			"type": "websocket.send",
			"text": repr(data)
		})

	async def websocket_receive(self, event):
		print("received", event)
		get_text = event.get('text', None)
		if get_text is not None:
			data = json.loads(get_text)

		self.create(data)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	@database_sync_to_async
	def list(self):
		queryset = Subject.objects.all()
		serializer = SubjectSerializer(queryset, many=True)
		return serializer.data

	def create(self, data):
		serializer = SubjectSerializer(data=data)
		if serializer.is_valid():
			serializer.save()
		return serializer.data

class SubjectConsumerUpdate(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Subject", event)
		await self.send({
			"type": "websocket.accept"
		})

	async def websocket_receive(self, event):
		print("received", event)
		get_text = event.get('text', None)
		if get_text is not None:
			data = json.loads(get_text)

		update_id = self.scope['url_route']['kwargs']['pk']
		self.update(data, update_id)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	def update(self, data, update_id):
		queryset = Subject.objects.filter(pk=update_id).first()
		serializer = SubjectSerializer(queryset, data=data)
		if serializer.is_valid():
			serializer.save()
		return serializer.data


class SubjectConsumerDelete(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Subject", event)
		await self.send({
			"type": "websocket.accept"
		})

		delete_id = self.scope['url_route']['kwargs']['pk']
		self.delete(delete_id)

	def delete(self, delete_id):
		return Subject.objects.filter(id=delete_id).delete()













#Teacher
class TeacherConsumerListCreate(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Teacher", event)
		await self.send({
			"type": "websocket.accept"
		})

		data = json.loads(json.dumps(await self.list()))
		await self.send({
			"type": "websocket.send",
			"text": repr(data)
		})

	async def websocket_receive(self, event):
		print("received", event)
		get_text = event.get('text', None)
		if get_text is not None:
			data = json.loads(get_text)

		self.create(data)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	@database_sync_to_async
	def list(self):
		queryset = Teacher.objects.all()
		serializer = TeacherSerializer(queryset, many=True)
		return serializer.data

	def create(self, data):
		serializer = TeacherSerializer(data=data)
		if serializer.is_valid():
			serializer.save()
		return serializer.data

class TeacherConsumerUpdate(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Teacher", event)
		await self.send({
			"type": "websocket.accept"
		})

	async def websocket_receive(self, event):
		print("received", event)
		get_text = event.get('text', None)
		if get_text is not None:
			data = json.loads(get_text)

		update_id = self.scope['url_route']['kwargs']['pk']
		self.update(data, update_id)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	def update(self, data, update_id):
		teacher = Teacher.objects.filter(pk=update_id).first()
		serializer = TeacherSerializer(teacher, data=data)
		if serializer.is_valid():
			serializer.save()

class TeacherConsumerDelete(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Teacher", event)
		await self.send({
			"type": "websocket.accept"
		})

		delete_id = self.scope['url_route']['kwargs']['pk']
		self.delete(delete_id)

	def delete(self, delete_id):
		return Teacher.objects.filter(id=delete_id).delete()









#Student
class StudentConsumerListCreate(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Student", event)
		await self.send({
			"type": "websocket.accept"
		})

		data = json.loads(json.dumps(await self.list()))
		await self.send({
			"type": "websocket.send",
			"text": repr(data)
		})

	async def websocket_receive(self, event):
		print("received", event)
		get_text = event.get('text', None)
		if get_text is not None:
			data = json.loads(get_text)

		self.create(data)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	@database_sync_to_async
	def list(self):
		queryset = Student.objects.all()
		serializer = StudentSerializer(queryset, many=True)
		return serializer.data

	def create(self, data):
		serializer = StudentSerializer(data=data)
		if serializer.is_valid():
			serializer.save()
		return serializer.data


class StudentConsumerUpdate(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Student", event)
		await self.send({
			"type": "websocket.accept"
		})

	async def websocket_receive(self, event):
		print("received", event)
		get_text = event.get('text', None)
		if get_text is not None:
			data = json.loads(get_text)

		update_id = self.scope['url_route']['kwargs']['pk']
		self.update(data, update_id)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	def update(self, data, update_id):
		queryset = Student.objects.filter(pk=update_id).first()
		serializer = StudentSerializer(queryset, data=data)
		if serializer.is_valid():
			serializer.save()
		

class StudentConsumerDelete(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Student", event)
		await self.send({
			"type": "websocket.accept"
		})

		delete_id = self.scope['url_route']['kwargs']['pk']
		self.delete(delete_id)

	def delete(self, delete_id):
		return Student.objects.filter(id=delete_id).delete()






#Class
class ClassConsumerListCreate(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Class", event)
		await self.send({
			"type": "websocket.accept"
		})

		data = json.loads(json.dumps(await self.list()))
		await self.send({
			"type": "websocket.send",
			"text": repr(data)
		})

	async def websocket_receive(self, event):
		print("received", event)
		get_text = event.get('text', None)
		if get_text is not None:
			data = json.loads(get_text)

		self.create(data)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	@database_sync_to_async
	def list(self):
		queryset = Class.objects.all()
		serializer = ClassSerializer(queryset, many=True)
		return serializer.data

	def create(self, data):
		serializer = ClassSerializer(data=data)
		if serializer.is_valid():
			serializer.save()
		return serializer.data

class ClassConsumerUpdate(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Class", event)
		await self.send({
			"type": "websocket.accept"
		})

	async def websocket_receive(self, event):
		print("received", event)
		get_text = event.get('text', None)
		if get_text is not None:
			data = json.loads(get_text)

		update_id = self.scope['url_route']['kwargs']['pk']
		info = self.update(data, update_id)
		await self.send({
			"type": "websocket.send",
			"text": repr(info)
		})

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	def update(self, data, update_id):
		classes = Class.objects.filter(pk=update_id).first()
		serializer = ClassSerializer(classes, data=data)

		get = Class.objects.filter(pk=update_id)
		get_serializer = ClassSerializer(get, many=True)
		final = json.loads(json.dumps(get_serializer.data))
		#valid = 0
		#for year in final[0]['student']:
		#	for tr in data['student']:
		#		if tr == year['id']:
		#			if year['year_level'] == final[0]['subject_details']['year_level']:
		#				valid = valid+1

		count = 0
		for x in final[0]['teacher_details']['can_teach']:
			if final[0]['subject'] == x:
  				count = count+1
  				
		if len(data['student']) > data['max_capacity']:
			return 'Class is Full!'
		elif count == 0:
			return 'Teacher cannot be assigned!'
		#elif valid == 0:
		#	return 'required year level does not match'
		elif serializer.is_valid():
			serializer.save()
			return 'saved!'



class ClassConsumerDelete(AsyncConsumer):
	async def websocket_connect(self, event):
		print("Connected to Class", event)
		await self.send({
			"type": "websocket.accept"
		})

		delete_id = self.scope['url_route']['kwargs']['pk']
		self.delete(delete_id)

	def delete(self, delete_id):
		return Class.objects.filter(id=delete_id).delete()
















#custom lists

class StudentClassesConsumerList(AsyncConsumer):
	async def websocket_connect(self, event):
		await self.send({
			"type": "websocket.accept"
		})

		get_id = self.scope['url_route']['kwargs']['pk']
		data = json.loads(json.dumps(self.list(get_id)))

		data_list = []
		for x in data:
			await self.send({
				"type": "websocket.send",
				"text": x['name']
			})

	async def websocket_receive(self, event):
		print("received", event)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	def list(self, get_id):
		queryset = Class.objects.all().filter(student=get_id)
		serializer = ClassSerializer(queryset, many=True)
		return serializer.data



class StudentTeacherConsumerList(AsyncConsumer):
	async def websocket_connect(self, event):
		await self.send({
			"type": "websocket.accept"
		})
		
		get_id = self.scope['url_route']['kwargs']['pk']
		data = json.loads(json.dumps(self.list(get_id)))
		print(data)
		for x in data:
			await self.send({
				"type": "websocket.send",
				"text": x['teacher_details']['last_name'] + ', ' + x['teacher_details']['first_name']
			})

	async def websocket_receive(self, event):
		print("received", event)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	def list(self, get_id):
		queryset = Class.objects.all().filter(student=get_id)
		serializer = ClassSerializer(queryset, many=True)
		return serializer.data


class ClassesStudentsConsumerList(AsyncConsumer):
	async def websocket_connect(self, event):
		await self.send({
			"type": "websocket.accept"
		})
		
		get_id = self.scope['url_route']['kwargs']['pk']
		data = json.loads(json.dumps(self.list(get_id)))

		for x in data:
			for y in x['student_details']:
				await self.send({
					"type": "websocket.send",
					"text": y['last_name'] + ', ' + y['first_name']
				})

	async def websocket_receive(self, event):
		print("received", event)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	def list(self, get_id):
		queryset = Class.objects.filter(pk=get_id)
		serializer = ClassSerializer(queryset, many=True)
		return serializer.data



class TeachersSubjectConsumerList(AsyncConsumer):
	async def websocket_connect(self, event):
		await self.send({
			"type": "websocket.accept"
		})
		
		get_id = self.scope['url_route']['kwargs']['pk']
		data = json.loads(json.dumps(self.list(get_id)))
		for x in data:
			await self.send({
				"type": "websocket.send",
				"text": x['teacher_details']['last_name'] + ', ' + x['teacher_details']['first_name']
			})


	async def websocket_receive(self, event):
		print("received", event)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	def list(self, get_id):
		queryset = Class.objects.all().filter(subject=get_id)
		serializer = ClassSerializer(queryset, many=True)
		return serializer.data



class StudentClassNumberList(AsyncConsumer):
	async def websocket_connect(self, event):
		await self.send({
			"type": "websocket.accept"
		})
		
		data = json.loads(json.dumps(self.list()))
		print(data)

		for x in data:
			num = len(x['student'])
			await self.send({
				"type": "websocket.send",
				"text": x['name'] + ' - ' + str(num)
			})

	async def websocket_receive(self, event):
		print("received", event)

	async def websocket_disconnect(self, event):
		print("disconnected", event)

	def list(self):
		queryset = Class.objects.all()
		serializer = ClassSerializer(queryset, many=True)
		return serializer.data


