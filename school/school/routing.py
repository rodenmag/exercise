from django.conf.urls import url
from django.urls import path, re_path
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator, OriginValidator

from system.consumers import *
application = ProtocolTypeRouter({
    'websocket': AllowedHostsOriginValidator(
    	AuthMiddlewareStack(
    		URLRouter ([
	    		path('course/', CourseConsumerListCreate),
	    		path('course/update/<int:pk>/', CourseConsumerUpdate),
	    		path('course/delete/<int:pk>/', CourseConsumerDelete),


	    		path('subject/', SubjectsConsumerListCreate),
	    		path('subject/update/<int:pk>/', SubjectConsumerUpdate),
	    		path('subject/delete/<int:pk>/', SubjectConsumerDelete),


	    		path('teacher/', TeacherConsumerListCreate),
	    		path('teacher/update/<int:pk>/', TeacherConsumerUpdate),
	    		path('teacher/delete/<int:pk>/', TeacherConsumerDelete),


	    		path('student/', StudentConsumerListCreate),
	    		path('student/update/<int:pk>/', StudentConsumerUpdate),
	    		path('student/delete/<int:pk>/', StudentConsumerDelete),


	    		path('class/', ClassConsumerListCreate), 
	    		path('class/update/<int:pk>/', ClassConsumerUpdate), #we can add and remove a student here. | max_capacity validation is working.
	    		path('class/delete/<int:pk>/', ClassConsumerDelete),





	    		#List of classes in which a particular student belongs 
	    		path('student_classes_list/<int:pk>/', StudentClassesConsumerList), #pk = primary key of student

	    		#List of required subjects a student needs to take for his/her course
	    		#none

	    		#List of teachers a student has for his/her classes
	    		path('student_teacher_list/<int:pk>/', StudentTeacherConsumerList), #pk = primary key of student


	    		#List of students for a particular class
	    		path('classes_students_list/<int:pk>/', ClassesStudentsConsumerList), #pk = primary key of class


	    		#List of teachers teaching a particular subject
	    		path('teachers_subject_list/<int:pk>/', TeachersSubjectConsumerList), #pk = primary key of subject


	    		#List of courses a particular subject is being required
	    		#none


	    		#List of all classes. Each class in the list should contain the number of students who take the class
	    		#path('number_of_student_in_class_list/', StudentClassNumberList),

	    	#List of all courses. Each course in the list should contain the following:
	    		#Number of students enrolled to the course
	    		#path('number_of_student_enrolled/', StudentCourseNumberList),

	    		#Number of teachers who teach subjects under the course

	    	])
    	)
    )
})


#ws://domain/int
